import NodeManager as NM
from time import sleep


def Scenario_1():
    nd = NM.RosNode("map_server", "map_server", args="/home/dogu/dogu_system/testmap.yaml")
    nd.launch()

    sleep(2)

    if NM.GetNodeStatus("/map_server*"):
        NM.KillNodes(["/map_server*"])


def Scenario_2():
    nd = NM.RosNode("map_server", "map_server", args="/home/dogu/dogu_system/testmap.yaml")
    nd.launch()

    sleep(2)

    nd.stop()


if __name__ == '__main__':
    Scenario_1()
    Scenario_2()
